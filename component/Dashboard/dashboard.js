import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardFragment from "../Fragments/dashboard-fragment/dashboard-fragment";
import ManageUsers from "../Fragments/manage-users-fragment/manage-users";
import styles from "./dashboard.module.css"
import ManageSpaces from "../Fragments/manage-spaces-fragment/manage-spaces";
import PaymentInvoices from "../Fragments/payment-invoices-fragment/payment-invoices";
import DocumentsFragment from "../Fragments/documents-fragment/documents-fragment";
import TicketsFragment from "../Fragments/Tickets-fragment/tickets-fragment";
import BookingFragment from "../Fragments/bookings-fragment/booking-fragment";
import Reviews from "../Fragments/reviews-fragment/reviews";
import FeaturedSpaceManagement from "../Fragments/featured-space-management-fragment/featured-space-management";
import AddManagement from "../Fragments/add-management-fragement/add-management-fragment";
import ManagementRfq from "../Fragments/management-rfq-fragment/management-rfq-fragement";
import LoadDashboard from "../Fragments/load-dashboard/load-dashboard";
import InteroperabilityFragment from "../Fragments/interoperability-fragment/interoperability-fragment";
import CommunityFragment from "../Fragments/community-fragment/community-fragment";
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ManageCustomers from "../Fragments/manage-customers-fragment/manage-customers";
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';



const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,

    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerContainer: {
        overflow: 'auto',
        outline:'none',
        backgroundColor:"#002366",
        color:"white",

    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },

}));

export default function Dashboard() {
    const classes = useStyles();
    const [fragment , setfragment] = useState("Dashboard");



     const loadFragment = () =>{
         switch (fragment){
             case "Dashboard":
                 return <DashboardFragment/>
                 case "Manage_Users":
                 return <ManageUsers/>
             case "Manage_Customers":
                 return <ManageCustomers/>
             case "Manage_Spaces":
                 return <ManageSpaces/>
             case "Payment_Invoices":
                 return <PaymentInvoices/>
             case "Documents":
                 return <DocumentsFragment/>
             case "Tickets":
                 return <TicketsFragment/>
             case "Booking":
                 return <BookingFragment/>
             case "Reviews":
                 return <Reviews/>
             case "Featured_Space_Management":
                 return <FeaturedSpaceManagement/>
             case "Add_Management":
                 return <AddManagement/>
             case "Management_Rfq":
                 return <ManagementRfq/>
             case "Load_Dashboard":
                 return <LoadDashboard/>
             case "Interoperability":
                 return <InteroperabilityFragment/>
             case "Community":
                 return <CommunityFragment/>
                 default:

                 break;
         }

     }
     return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" noWrap>
                        Leasing Monk
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <Toolbar />
                <div className={classes.drawerContainer}>
                    <List>
                        <ListItem button
                                  onClick ={e => setfragment("Dashboard")} >
                                <ListItemIcon>

                                </ListItemIcon>
                                <ListItemText  primary="Dashboard" />
                            </ListItem>

                    </List>
                    <List>
                        <ListItem button
                                  onClick ={e => setfragment("Manage_Users")} >
                            <ListItemIcon>

                            </ListItemIcon>
                            <ListItemText primary="Manage Users" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button
                                  onClick ={e => setfragment("Manage_Customers")} >
                            <ListItemIcon>
                               {/*<PermIdentityIcon htmlColor="white"/>*/}
                            </ListItemIcon>
                            <ListItemText primary="Manage Customers" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Manage_Spaces")} >
                            <ListItemIcon>

                            </ListItemIcon>
                            <ListItemText primary="Manage Spaces" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Payment_Invoices")} >
                            <ListItemIcon>
                                {/*<InsertDriveFileIcon htmlColor="white"/>*/}
                            </ListItemIcon>
                            <ListItemText primary="Payment & Invoices" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Documents")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Documents" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Tickets")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Tickets" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Booking")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Bookings" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Reviews")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Reviews" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Featured_Space_Management")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Featured Space Management" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Add_Management")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Add. Management" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Management_Rfq")}>
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Manage RFQ" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Load_Dashboard")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Load Dashboard" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Interoperability")} >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Interoperability" />
                        </ListItem>

                    </List>
                    <List>
                        <ListItem button onClick ={e => setfragment("Community")}  >
                            <ListItemIcon></ListItemIcon>
                            <ListItemText primary="Community" />
                        </ListItem>

                    </List>


                </div>
            </Drawer>
            <main className={classes.content}>
                <Toolbar />
                {loadFragment()}
            </main>
        </div>
    );
}
